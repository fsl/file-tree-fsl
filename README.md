Filetree definitions for the FSL neuroimaging library.

Add new filetree definitions to the file_tree_fsl/trees directory.

After installation of these filetrees (`pip install file-tree-fsl`), 
you do not have to import this library. `file-tree` will detect
it automatically and these trees will be available to load directly
or as sub-trees.

For documentation see the main file-tree package (https://open.win.ox.ac.uk/pages/fsl/file-tree/)
and the FSL wiki (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/).